Development guide for the accessibility infrastructure
======================================================

.. toctree::
   at-spi3
   atk-deprecations
   de-controller
   gitlab-ci
   meeting-2023-01-13
   roadmap
   toolkits
   xml-changes
   xml-interfaces
   :maxdepth: 1
   :caption: Contents:

Welcome to the developer's guide for the freedesktop accessbility
infrastructure.  This guide intends to become a repository of
knowledge on the implementation of the accessibility infrastructure,
as well as a guide for application developers and toolkit developers
who want to make their user interfaces accessible.

If you want to modify this document, `please see its source code
<https://gitlab.gnome.org/GNOME/at-spi2-core/-/tree/main/devel-docs>`_.

General documentation
---------------------

- :doc:`roadmap`
- :doc:`toolkits`
- :doc:`xml-changes`
- :doc:`xml-interfaces`

Refactoring and cleanup
-----------------------

- :doc:`de-controller`
- :doc:`atk-deprecations`
- :doc:`at-spi3`

Information for maintainers
---------------------------

- :doc:`gitlab-ci`

Meeting minutes
---------------

- :doc:`meeting-2023-01-13`
